#!/usr/bin/env ruby
require 'net/http'
require 'openssl'

appId = ENV['GOOGLE_PLAY_ID'] || 'com.apploi'

SCHEDULER.every '10m', :first_in => 0 do |job|

  # prepare request
  http = Net::HTTP.new("play.google.com", Net::HTTP.https_default_port())
  http.use_ssl = true
  http.verify_mode = OpenSSL::SSL::VERIFY_NONE # disable ssl certificate check

  # scrape detail page of app with appId
  response = http.request(Net::HTTP::Get.new("/store/apps/details?id=#{appId}"))
  if response.code != "200"
    puts "google play store website communication (status-code: #{response.code})\n#{response.body}"
  else
    data = { 
      average_rating: 0.0,
      voters_count: 0
    }

    # capture average rating using regexp
    average_rating = /class=[\"\']score[\"\']>([\d,.]+)</.match(response.body)
    average_rating = average_rating[1].gsub(",", ".").to_f
    data[:average_rating] = average_rating

    # capture total number of votes
    voters_count = /class=[\"\']reviews-num[\"\']>([\d,.]+)</.match(response.body)
    voters_count = voters_count[1]
    data[:voters_count] = voters_count

    send_event('google_play_store_rating', data) 
  end
end
