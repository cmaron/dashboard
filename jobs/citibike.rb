require 'net/http'
require 'uri'
require 'json'


SCHEDULER.every '5m', :first_in => 0 do |job|

  begin
    uri = URI("http://www.citibikenyc.com/stations/json")
    http = Net::HTTP.new(uri.host, uri.port)
    http.use_ssl = false
    http.verify_mode = OpenSSL::SSL::VERIFY_NONE
    request = Net::HTTP::Get.new(uri.request_uri)

    response = http.request(request)
    data = JSON.parse(response.body)

    stations = data['stationBeanList'].map do |row|
      row = {
        :id => row['id'],
        :name => row['stationName'],
        :status => row['statusValue'],
        :bikes_available => row['availableBikes'],
        :docks_available => row['availableDocks'],
        :total_docks => row['totalDocks']
      }
    end

    stations.each { |station|
      event_name = 'citibike_'+station[:id].to_s
      send_event(event_name, station)
    }

  rescue
    puts 'citibike error'
    # puts response.body
  end

end