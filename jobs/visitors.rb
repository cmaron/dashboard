require 'google/api_client'
require 'date'

client = Google::APIClient.new(
  :application_name => 'Apploi Users Real-Time',
  :application_version => '1.0.0'
)

iosProfileID = '79509262'
comProfileID = '87780067'
corpProfileID = '81908059'
grailsProfileID = '70155360'

profiles = [{name: 'iosvisitors', id: '79509262'},
                    {name: 'comvisitors', id: '87780067'},
                    {name: 'grailsvisitors', id: '70155360'},
                    {name: 'corpvisitors', id: '81908059'}]

visitors = {}
profiles.each do |profile|
  visitors[profile[:name]] = []
end

service_account_email = '901402520488-2qd0uku05aknjrva91t89fnr7ee3rdsg@developer.gserviceaccount.com'
key_file = './jobs/f53d942747fb89d3d962b0d80734277a1074576e-privatekey.p12'
key_secret = 'notasecret'

# Load your credentials for the service account
key = Google::APIClient::KeyUtils.load_from_pkcs12(key_file, key_secret)
client.authorization = Signet::OAuth2::Client.new(
:token_credential_uri => 'https://accounts.google.com/o/oauth2/token',
:audience => 'https://accounts.google.com/o/oauth2/token',
:scope => 'https://www.googleapis.com/auth/analytics.readonly',
:issuer => service_account_email,
:signing_key => key)

# The number of profiles limits how many requests can be made to stay under the 50k/day quota.
# 34.72222222222222 = 50000/24.0/60.0, so use 34 
run_every = ((34/profiles.length).floor)
puts "Will refresh active users every #{run_every} seconds"

SCHEDULER.every "#{run_every}s" do
  # Request a token for our service account
  client.authorization.fetch_access_token!

  # Get the analytics API
  analytics = client.discovered_api('analytics','v3')

  # Execute the queries
  profiles.each do |profile|
    response = client.execute(:api_method => analytics.data.realtime.get, :parameters => {
      'ids' => "ga:" + profile[:id],
      'metrics' => "rt:activeUsers",
    })
    if response.error?
      puts response.error_message
    elsif response.data.rows.nil? or response.data.rows[0].nil?
      puts "No data for " + profile[:name]
    else
      visitors[profile[:name]] << { x: Time.now.to_i, y: response.data.rows[0][0].to_i }
      send_event(profile[:name], points: visitors[profile[:name]])
    end
    # Can only have one request per second...
    sleep(2)
  end
end

