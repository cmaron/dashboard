require 'dashing'

configure do
  set :auth_token, '736FE955-2744-48DC-8706-D63E11127497'

  helpers do
    def protected!
     # Put any authentication code you want in here.
     # This method is run before accessing any resource.
    end
  end
end

map Sinatra::Application.assets_prefix do
  run Sinatra::Application.sprockets
end

set :protection, false

# use Rack::Auth::Basic, "Tech Dashboard" do |username, password|
#  username == 'tech' and password == 'apploi123'
# end

# http://stackoverflow.com/questions/4351904/sinatra-options-http-verb
configure do
  class << Sinatra::Base
    def options(path, opts={}, &block)
      route 'OPTIONS', path, opts, &block
    end
  end
  Sinatra::Delegator.delegate :options
end

options '*' do
  response.headers["Access-Control-Allow-Headers"] = "origin, x-requested-with, content-type"
  response.headers["Access-Control-Allow-Origin"] = "*"
  response["Access-Control-Allow-Origin"] = "*"
  response.headers["Access-Control-Allow-Methods"] = "POST"

  halt 200
end

run Sinatra::Application
