## Description

Simple [Dashing](http://shopify.github.com/dashing) widget (and associated job) to display [Pingdom](http://pingdom.com) checks.

##Dependencies

[rest-client](http://rubydoc.info/gems/rest-client/1.6.7/frames)

Add it to dashing's gemfile:

    gem 'rest-client'
    
and run `bundle install`. Everything should work now :)

##Usage

To use this widget, copy `pingdom.html`, `pingdom.coffee`, and `pingdom.scss` into the `/widgets/pingdom` directory. Put the `pingdom.rb` file in your `/jobs` folder.

To include the widget in a dashboard, add the following snippet to the dashboard layout file:

    <li data-row="1" data-col="1" data-sizex="1" data-sizey="1">
      <div data-id="pingdom" data-view="Pingdom" data-title="Pingdom" data-cols="1"></div>
    </li>

##Settings

You'll need to add your pingdom API key and login settings. You can either add it straight to the source code on lines 4-6 of pingdom.rb, or set the variables in the ENV. For heroku, you can do this with

    heroku config:set PINGDOM_API_KEY=
    heroku config:set PINGDOM_USER=
    heroku config:set PINGDOM_PASSWORD=

Pingdom is checked every minute, but you can change that by editing the job schedule.

## Contributions

This has been extracted from a dashboard for the [UK Ministry of Justice](http://blogs.justice.gov.uk/digital/), with minimal generalising. Feel free to submit patches to @james.

Modified by @jwalton to use rest-client instead of pingdom-client.