class Dashing.Citibike extends Dashing.Widget

    @accessor 'backgroundClass', ->
        if @get('status') == "In Service"
            "citibike-status-active"
        else if @get('status') == "Not In Service"
            "citibike-status-unavailable"
        else
            "citibike-status-unknown"

    constructor: ->
        super

    ready: ->
        $(@node).addClass(@get('backgroundClass'))


    onData: (data) ->
        $(@node).addClass(@get('backgroundClass'))