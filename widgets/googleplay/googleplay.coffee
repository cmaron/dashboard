class Dashing.Googleplay extends Dashing.Widget
  ready: ->
    @onData(this)
 
  onData: (data) ->
    widget = $(@node)

    rating = @get('average_rating')
    voters_count = @get('voters_count')

    widget.find('.googleplay-rating-value').html( '<div>Average Rating</div><span id="googleplay-rating-integer-value">' + rating + '</span>')
    widget.find('.googleplay-voters-count').html( '<span id="googleplay-voters-count-value">' + voters_count + '</span> Votes' )
